<html>
<head>
<head>
<link rel="stylesheet" type="text/css" href="default.css">
<meta charset="utf-8">
<meta name="description" content="Author: Raffaele Arecchi, Specification: FootnoteML 1.0, Category: Markup Languages">
</head>

<body lang="en">
<div id="content" class="inner">

<h1 class="top">Footnote Markup Language version 1.0</h1>
<p>Author: Raffaele Arecchi<br/>
Creation date: 23/04/2017<br/>
Last update: 10/09/2017</p>


<p>This document describes the Footnote Markup Language version 1.0. <br/> This work is licensed under a
<a rel="license" href="http://creativecommons.org/licenses/by-sa/4.0/">Creative Commons Attribution-ShareAlike 4.0 International License</a>.
</p>
<p><b>Contents</b>:
<ol>
<li><a href="#rationale">Rationale</a></li>
<li><a href="#footnotes">Footnotes</a></li>
<li><a href="#specification">Specification</a></li>
<li><a href="#final">Final remarks</a></li>
<li><a href="#references">Non digital references</a></li>
</ol>
</p>

<h3><a id="rationale">Rationale</a></h3>
<p>The Footnote Markup Language, throughout this document abbreviated FootnoteML, is a 
<a href="https://en.wikipedia.org/wiki/Markup_language">markup language</a> designed after
the following principles:
<ol>
	<li>The text should be readable and the markup less verbose and less invasive as possible</li>
	<li>The text should be unstructured and the tags free to place</li>
	<li>The possibility to add multiple semantics to text, allowing the tags to
<a href="https://en.wikipedia.org/wiki/Overlapping_markup">overlap</a></li>
	<li>A clear distinction between semantic metadata (tags) and semantic visualization (browser)</li>
</ol>
</p>
<p>
The ability to add tags and leave a document easily readable and editable is an important feature of a markup language
and FootnoteML readability competes with those of other markup languages.
</p>
<p>
FootnoteML does not have any requirement about structuring of the text and leaves the author the full freedom to express any
thinking without the contraint of a data structure in mind. Many other markup languages tipically require a tree
structuring of the document.
</p>
<p>This structuring freedom and the allowance of tag overlapping enable FootnoteML to add multiple semantics to a text.
The value of this feature can be show with a little example, consider the following verses from John Keats' poem Endymiom:
<div class="exampleInner">
<pre>A thing of beauty is a joy for ever:
Its loveliness increases; it will never
Pass into nothingness; but still will keep
A bower quiet for us, and a sleep</pre></div><br/>
An intellectual reviewer would be happy to add rich semantic to this: she would state that each line is a verse but 
also she would give an explanation for <i>"it will never Pass into nothingness"</i> that overlaps two verses.
Many markup languages fail to represent and distinguish at the same time the metric structure
from the paraphrasal structure. <a href="https://www.w3.org/XML/">XML</a> for example, could be
theoretically extended to allow multiple DTD in the same text, but overlapping tags that belong to the same DTD will
cause the text to be XML invalid, unless extending it further introducing IDs <a href="#biblio_SJDeRose">[SJDeRose]</a>.
</p>

<p>
The last aspect aims to enforce the separation of the document visualization from its semantics.
This is essential on overlapping structures as they require different semantic visualization.
In this context, many markup languages suffer from the intrusiveness of visualization tag elements.
</p>

<h3><a id="footnotes">Footnotes</a></h3>
<p>The main idea in FootnoteML comes from the primordial of all kinds of metadata: the footnote. Here is an example:</p>
<div class="exampleInner">
<pre>Be still my heart; thou¹ hast known worse than this²³.

	¹ you (archaic)
	² you had worse than this to bear
	³ see Homer, Odissey - Book XX</pre></div>
<p>
The nice thing about footnotes is that they have no rules, but everyone is (usually) able to understand their scope and meaning.<br/>
The first note refers to a single word, <i>thou</i>, and its linking to the text could be interpreted as <i>"thou is synonym for you"</i>.
The second note has a wider scope and represents a modern translation. The third note refers to the entire phrase
and is a reference.
</p>
<p>The metadata scope and meaning can be made clearer by superscript delimitation and verbs in the notes:</p>
<div class="exampleInner">
<pre>³Be still my heart; ²¹thou¹ hast known worse than this²³.

	¹ is synonym for       →  you (archaic)
	² means                →  you had worse than this to bear
	³ is a reference from  →  Homer, Odissey - Book XX</pre></div>
<p>FootnoteML takes the same idea by:</p>
<ol>
	<li>Delimiting scope by numbered tags,</li>
	<li>Express the note in the form <i>subject-predicate-object</i>, the subject being the source text.</li>
</ol>
<p>A good feature of such markup language is that we do not need to structure the text at all, 
and we can place the metadata on a separate line to improve readability and editability.</p>
<p>Happily, this markup admits tag overlapping: in the last example the tags 1 and 2 are unrelated
and the same interpretation is given if we swap <code>²¹thou¹</code> with <code>¹²thou¹</code>.</p>
<p>While the complete disconnection of numbered tags is what makes overlapping possible, the
drawback is that tag classification becomes cumbersome. The <a href="https://www.w3.org/TR/html4/">HTML</a>
<div class="exampleInner">
<pre>&#60;p&#62;Today is a nice day.&#60;/p&#62;
&#60;p&#62;Hope tomorrow will be the same.&#60;/p&#62;
</pre></div><br/>
would be equivalent to<br/><br/>
<div class="exampleInner">
<pre>¹Today is a nice day.¹
²Hope tomorrow will be the same.²

	¹ is a → paragraph 
	² is a → paragraph
</pre></div><br/>

and many paragraphs will generate lots of duplicated information.
It is easy to solve this by reuse of the same footnote:<br/><br/>
<div class="exampleInner">
<pre>¹Today is a nice day.¹
¹Hope tomorrow will be the same.¹

	¹ is a → paragraph 
</pre></div><br/>
However, in the case of same-number overlapping, two different numbers will be anyway required
otherwise tag boundaries will not be interpretable.
But again this can be improved if the footnotes are allowed to inherit content from other footnotes:
just rewrite<br/><br/>
<div class="exampleInner">
<pre>Peter Piper ¹picked ²a peck¹ of pickled² peppers.

	¹ is an → alliteration
	² is an → alliteration
</pre></div><br/>
into<br/><br/>
<div class="exampleInner">
<pre>Peter Piper ¹picked ²a peck¹ of pickled² peppers.

	¹ inherits from ³
	² inherits from ³
	³ is an → alliteration
</pre></div></p>
<p>
	The syntax gets more compact if we introduce an inheritance notation (<b>:</b>) and if the footnote can be written
	once and then referenced beyond in the text:
<div class="exampleInner">
<pre>	³ is an → alliteration

Peter Piper ¹picked ²a peck¹ of pickled² peppers.
	
	¹:³
	²:³
</pre></div></p>
<p>More generally, footnote inheritance can be useful to maintain a low number of organized notes.
</p>
<p>As last observation, we can allow the content inside the metadata to be, in turn, tagged to a footnote within the same
text file, a feature that transcends the common markup practise:
<div class="exampleInner">
<pre>³Be still my heart; ²¹thou¹ hast known worse than this²³.

	¹ is ⁴synonym⁴ for     →  you (archaic)
	² means                →  ⁵you had worse than this to bear⁵
	³ is a reference from  →  ⁶Homer⁶, Odissey - Book XX
	⁴ more on              →  http://en.wikipedia.org/wiki/Synonym
	⁵ paraphrase by        →  Adso from Melk, 1382
	⁶ is                   →  an ancient greek poet whose existence is disputed</pre></div>
<br/>
To maintain this meta-meta tagging as general as possible, we will consider infinitely recursive
references meaningless but valid:<br/><br/>
<div class="exampleInner">
<pre>Hope one day the GNU ¹Hurd¹ microkernel will become stable.

	¹ is acronym for     →  ²Hird² of Unix-Replacing Daemons
	² is acronym for     →  ¹Hurd¹ of Interfaces Representing Depth</pre></div>
</p>

<h3><a id="specification">Specification</a></h3>
<p>A FootnoteML file is any kind of text file with <a href="https://en.wikipedia.org/wiki/Unicode">Unicode</a>
characters. No specific encoding and no file extensions are strictly required.</p>
<p>
A tag is represented by an integer number sorrounded by <i>tilde</i>s <b>~</b>.
An opening tag with a missing closing one, or viceversa, should be considered
invalid. Escaping of tilde is performed by doubling it.</p>

<p>Here is an example of valid tags:</p>
<div class="exampleInner">
<pre>~3~Be still ~4~my heart; ~2~~1~thou~1~ hast known~4~ worse than this~2~~3~.</pre></div>

<p>A line whose first character is the <i>sharp</i> <b>#</b>, excluding <i>blank</i>s and <i>tab</i>s, it represents a note.
 The <i>sharp</i> must be followed immediately by the number to which it is referring, then followed by eventual inheritance content,
then followed by the metadata content of the note. </p>

<p>Notes can be placed anywhere in the text.</p>

<p>The <b>inheritance content</b> of the note is optional, if present it must start with a <i>colon</i> <b>:</b>
followed by a sequence of numbers separated by <i>comma</i>s <b>,</b>.
A note showing an inheritance content is equivalent to the same note but including as metadata content
also all the metadatas of the numbered notes to which the inheritance content is referring to, recursively.</p>
<p>
The <b>metadata content</b> of the note is composed by an unlimited sequence of predicate-object couples marked by
<i>square braket</i>s. Inside the brackets, the predicate and the object are separated by <i>pipe</i> <b>|</b>
while interspace outside the brackets is not relevant. Predicate and object content are allowed to be tagged
just any other text, and to be displayed on multiple lines, but are not allowed to contain subnotes.</p>
<p>
Escaping inside a note is performed by backslash <b>\</b>, and <b>\\</b> represents the backslash itself.</p>

<p>Here is an example of valid notes:</p>
<div class="exampleInner">
<pre>#7 [is ~44~synonym~44~ for|you] [is historically|archaic]
#9:34,1,2 [means|you had worse than this to bear]
#5 [is a reference from|~13~Homer~13, ~72~Odissey~72~ - Book XX]
#13 [is|an ancient greek poet
          whose existence is disputed]
#44 [more on|http://en.wikipedia.org/wiki/Synonym]
#72 [more on|
             http://en.wikipedia.org/wiki/Odyssey]</pre></div>

<p>A note number can be redefined, but this must be interpreted as if the number is a new generated number that does not
appear anywhere in the text. All tags and notes that are located after the note redefinition and refere to this number
(by reference or inheritance) must be interpreted as if they refere to the new generated number.
</p>

<p>The predicate <b>is</b> can be omitted.</p>

<h3><a id="final">Final remarks</a></h3>
<p>FootnoteML is similar to HTML, but more expressive. A nice feature of FootnoteML is that the text layout remains stable
and familiar to paper books.</p>
<p>To improve readability further, all the notes can be placed after a <i>tab</i> character.</p>
<p>To improve editing, an editor program could simply blur the tags by a ligher color, or minimizing them,
and highlights matching opening/closing tags by darker colors. Advanced editing could include note
number refactoring and metadata tooltip for inherited notes.</p>
<p>The Endymiom could by shown as:</p>
<div class="exampleInner">
<pre>	#1 [is metrically a|verse]

A thing of beauty is a joy for ever:
<span class="fttag">~2~</span>Its loveliness increases; <span class="fttag">~4~</span>it will never<span class="fttag">~2~</span>
<span class="fttag">~3~</span>Pass into nothingness<span class="fttag">~4~</span>; but still will keep<span class="fttag">~3~</span>
A bower quiet for us, and a sleep

	#2:1 [rhymes with|ver]
	#3:1 [rhymes with|eep]
	#4 [means|<span class="fttag">~5~</span>it will live forever<span class="fttag">~5~</span>]
	#5 [paraphrase by|Adso from Melk, 1382]</pre></div>

<p>If the notes are encoded to a standard, a browser then could be enabled to let the intellectual user
 select the view modality: metric or logic.</p>
 
<p>Ona a different context, the following example shows how FootnoteML is suitable for tagging work notes in a file:</p>
<div class="exampleInner">
<pre>  #1 [malaria] [cause] [italy] [02/04/2016]

~1~
Today report from Liam Brown showed that Malaria in Italy is mainly an import disease due to the increase in migratory flows
and trips to endemic tourism or VFR (visiting friends and relatives). It is the main cause of fever when returning from tropical
countries and should be considered the first suspect diagnosis in case of febrile subjects coming from endemic areas.
Approximately 600 cases per year (30% Italian citizens, 70% foreign citizens) a 5-10% share develops serious malaria, a medical
emergency characterized by multiorgan involvement with 40% mortality in the event of no diagnosis or absence of treatment.
~1~

  #1 [ABC] [anomaly] [dam]
  #2 [ABC] [test] [log]
  #3 [command] [download]

~1~
The ABC application is in charge of detecting anomalies in seepage paths in the earth dam.
~2~
Log in test environment are located at http://acme-intranet.es/test/abc/log
~2~
~3~
Example of download from the command line: wget -e use_proxy=no http://acme-intranet.es/test/abc/log/tracing.log
~3~
~1~</pre></div>


<h3><a id="references">Non digital references</a></h3>
<a id="biblio_SJDeRose">[SJDeRose]</a> <i>S.J.DeRose - The SGML FAQ Book: Understanding the Foundation of HTML and XML.
 Section 5.9: Can I make an element that crosses element boundaries (in the rare case when I need to)?</i>

<hr>
<div style="margin-top: 10px;">
	<a href="https://creativecommons.org/licenses/by-sa/2.0/"> <img
		src="CC-BY-SA_icon.svg"
		style="width: 100px; float: left; margin: 0 10 10 0px;" />
	</a> Author: Raffaele Arecchi. Contact the author of this document via
	mail to: <b>raffaele.arecchi</b> <b>{at}gmail{point}com</b> <br>
	This work is licensed under a <a rel="license"
		href="http://creativecommons.org/licenses/by-sa/4.0/">Creative
		Commons Attribution-ShareAlike 4.0 International License</a>.
</div>

</div>
</body>
</html>

